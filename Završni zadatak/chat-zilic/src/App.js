import React, { Component } from "react";
import "./App.css";
import Input from "./Components/Input";
import Messages from "./Components/Messages";

function randomUsername() {
  const users = ["Ivo", "Marko", "Petar", "Ante", "Josip", "Luka"];

  const user = users[Math.floor(Math.random() * users.length)];
  return user;
}
function randomUserImg(){
  let random = (Math.random() * (100-1)).toFixed(0);
  const img = `https://randomuser.me/api/portraits/thumb/men/${random}.jpg`;
  return img;
}

export default class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      messages: [],
      member: {
        username: randomUsername(),
        img: randomUserImg()
      }
    };

    this.drone = new window.Scaledrone("Lbw7I83eHxqGhEgh", {
      data: this.state.member,
    });
    this.drone.on("open", (error) => {
      if (error) {
        return console.error(error);
      }
      const member = { ...this.state.member };
      member.id = this.drone.clientId;
      this.setState({ member });
    });

    const room = this.drone.subscribe("observable-room");
    room.on("data", (data, member) => {
      const messages = this.state.messages;
      messages.push({ member, text: data });
      this.setState({ messages });
      console.log(this.state.member)
    });
  }

  InputMessage = (message) => {
    this.drone.publish({
      room: "observable-room",
      message,
    });
  };

  render() {
    return (
      <div className="container">
        <div className="header">
          <h1>Dobrodošli na chat</h1>
        </div>
        <Messages
          messages={this.state.messages}
          currentMember={this.state.member}
        />
        <Input InputMessage={this.InputMessage} />
      </div>
    );
  }
}



