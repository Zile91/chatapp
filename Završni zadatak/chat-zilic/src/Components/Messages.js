import React, { Component } from "react";
import PropTypes from 'prop-types';

export default class Messages extends Component {
    renderMessage(message, i) {

    const messageFromMe = message.member.id === this.props.currentMember.id;
    const className = messageFromMe ? "currentMember" : "message";

    return (
        <ul className={className} key={i}>
             <div className="username">
              <p>{message.member.clientData.username}
              <img src={message.member.clientData.img} 
                   alt={message.member.clientData.username}/></p>
            </div>
            <div className="messageText">
                <p>{message.text}</p>
            </div>
        </ul>
    );
  }

  render() {
    return (
      <ul className="listMessages">{this.props.messages.map((message, i) => this.renderMessage(message, i))}</ul>
    );
  }
}
Messages.propTypes = {
    messages: PropTypes.array,
    currentMember: PropTypes.object
}